import boto3
from botocore.exceptions import ClientError
  
iam = boto3.client('iam')
sts = boto3.client('sts')

# Get the arn represented by the currently configured credentials
arn = sts.get_caller_identity()['Arn']

# Run the policy simulation for the basic s3 operations
try:
    iam.simulate_principal_policy(
        PolicySourceArn=arn,
        ResourceArns=[],
        ActionNames=['kms:ImportKeyMaterial']
    )
except ClientError as e:
    if e.response['Error']['Code'] == "AccessDenied":
        print("permission denied")
        return False
    else:
        # TODO: handle other errors there
        pass
