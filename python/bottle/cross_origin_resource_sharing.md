# CORS Explain

Reference: https://ongspxm.github.io/blog/2017/02/bottlepy-cors/

# Enable CORS

1. create decorator
2. add OPTIONS method for APIs

```python
# create decorator
def enable_cors(func):
    def wrapper(*args, **kwargs):
        bottle.response.set_header("Access-Control-Allow-Origin", "*")
        bottle.response.set_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        bottle.response.set_header("Access-Control-Allow-Headers", "Origin, Content-Type")

        # skip the function if it is not needed
        if bottle.request.method == 'OPTIONS':
            return

        return func(*args, **kwargs)
    return wrapper


# OPTIONS will be automatically generated for this API by adding 'OPTIONS' method
@bottle.route('/api/jobs', method=['GET', 'OPTIONS'])
@enable_cors
def api_jobs_list():
	return json.dumps(result)
```
