# Retry on Requests

Implement retry with urllib3 and hook in Requests session

```python
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
    

def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session
```

## Usage

```python
s = requests.Session()

# basic auth
s.auth = ('user', 'pass')

# update headers
s.headers.update({'x-test': 'true'})
    
response = requests_retry_session(session=s).get(
    'https://www.example.com'
)
```
