# Logging

# Quick Start

## YAML Configuration

```yaml
---
version: 1
disable_existing_loggers: False
formatters:
    simple:
        format: "%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s"

handlers:
    console:
        class: logging.StreamHandler
        level: DEBUG
        formatter: simple
        stream: ext://sys.stdout

    info_file_handler:
        class: logging.handlers.RotatingFileHandler
        level: INFO
        formatter: simple
        filename: info.log
        maxBytes: 104857600 # 100MB
        backupCount: 10
        encoding: utf8

    error_file_handler:
        class: logging.handlers.RotatingFileHandler
        level: ERROR
        formatter: simple
        filename: error.log
        maxBytes: 104857600 # 100MB
        backupCount: 10
        encoding: utf8

loggers:
    my_logger:
        level: DEBUG
        handlers: [console, info_file_handler, error_file_handler]
        propagate: no

#root:
#    level: INFO
#    handlers: [console, info_file_handler, error_file_handler]
```

## Load Configuration in Python

```python
import logging.config
import yaml


def get_logger(logger_name='root'):
    log_conf = 'logging.yml'
    with open(log_conf, 'r') as f:
        config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)
    return logging.getLogger(logger_name)

logger = get_logger('my_logger')
logger.debug('debug')
logger.info('info')
logger.warning('warning')
logger.error('error')

logger_root = get_logger()
logger_root.debug('debug')
logger_root.info('info')
logger_root.warning('warning')
logger_root.error('error')
```

# AWS CloudWatch

## Installation

```bash
pip install watchtower
```

## Send Logs to CloudWatch

Add new handlers in section:

```yaml
handlers:
    aws_cloudwatch:
        class: watchtower.CloudWatchLogHandler
        level: DEBUG
        formatter: simple
        log_group: log_group_name
        stream_name: log_stream_name
        send_interval: 1
        create_log_group: True  # True by default
        boto3_profile_name: aws_profile_watchtower  # optional
```

## Read Logs from CloudWatch

```bash
aws logs get-log-events --log-group-name log_group_name --log-stream-name log_stream_name | jq '.events[].message'
```

## Filter Logs on AWS CloudWatch

Log message can be parsed in AWS CloudWatch service. (CloudWatch -> Logs -> Insights)

```text
parse @message '* * - * - * - * - *' as date, time, level, code, module, content
| sort @timestamp desc
| filter level == 'ERROR'
| limit 20
```

## Reference

- [Watchtower Github](https://github.com/kislyuk/watchtower)
- [Watchtower Documentation](https://watchtower.readthedocs.io/en/latest/#module-watchtower)