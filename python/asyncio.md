# Quick Start

``` python
import asyncio

async def my_function(input):
    print("%s start" % input)
    await asyncio.sleep(3)  # simluate I/O behavior
    print("%s end" % input)
    return input
    
asyncio.set_event_loop(asyncio.new_event_loop())  # so get_event_loop can get new loop
loop = asyncio.get_event_loop()
tasks = []
for i in range(3):
    tasks.append(asyncio.ensure_future(my_function(i)))
    
done_tasks, pending_tasks = loop.run_until_complete(asyncio.wait(tasks))
loop.close()
    
# print out return content
for future in done_tasks:
    value = future.result()
    print("return: {}".format(value))
```

Execution output:

```
0 start
1 start
2 start
(3 seconds sleep)
0 end
1 end
2 end
return 0
return 2
return 1
```

> Reference:
> http://djangostars.com/blog/asynchronous-programming-in-python-asyncio/


# Async Requests (aiohttp)

``` python
import asyncio
from aiohttp import ClientSession

async def fetch(url, session):
    async with session.get(url) as response:
        return await response.read()
    
async def fetch_post(url, post_data session):
    async with session.post(url, data=post_data) as response:
        return await response.read()
            
async def run(urls):
    tasks = []
    async with ClientSession() as session:
        for url in urls:
            task = asyncio.ensure_future(fetch(url, session))
            tasks.append(task)
        responses = await asyncio.gather(*tasks)
    return responses
    
urls = [
    "http://example.org/",
    "http://google.com/"
]
asyncio.set_event_loop(asyncio.new_event_loop())
loop = asyncio.get_event_loop()
responses = loop.run_until_complete(run(urls))
loop.close()
```

# Async Download Images (aiohttp)

``` python
import asyncio
from aiohttp import ClientSession

def download_images(urls):
    asyncio.set_event_loop(asyncio.new_event_loop())
    loop = asyncio.get_event_loop()
    loop.run_until_complete(download_images(urls))
    loop.close()
    
async def _download_images(urls):
    tasks = []
    async with ClientSession() as session:
        serial_number = 0
        for url in urls:
            serial_number += 1
            url_extension = url[url.rfind('.')+1:]
            output_file = "tmp/{:03}.".format(serial_number) + url_extension
            task = asyncio.ensure_future(_download_to_local(session, url, output_file))
            tasks.append(task)
        responses = await asyncio.gather(*tasks)

async def _download_to_local(session, url, filename):
    async with session.get(url) as response:
        image = await response.read()
        with open(filename, 'wb') as f:
            f.write(image)
```

# Execute Sync Fuction in Async

```python
"""
Quick guide to make synchronous python code executed in asynchronous mode.
Best solution is to use asynchronous library, this is just a quick hack.
"""

import asyncio
import time


def output(msg):
    ts = time.strftime("%H:%M:%S")
    print("{} - {}".format(ts, msg))


def sync_function(t):
    # simluate a sync function which will end after t seconds
    output("sync_function({}) start".format(t))
    time.sleep(t)
    output("sync_function({}) end".format(t))
    return t


def async_with_different_functions():
    """execute different functions asynchronously
    """
    loop = asyncio.new_event_loop()
    future1 = loop.run_in_executor(None, sync_function, 5)
    future2 = loop.run_in_executor(None, sync_function, 3)
    # The order of result values corresponds to the order of futures
    results = loop.run_until_complete(asyncio.gather(future1, future2, loop=loop))
    loop.close()
    return results


def async_with_same_function():
    """execute same functions with different parameters asynchronously
    """
    loop = asyncio.new_event_loop()
    futures = [loop.run_in_executor(None, sync_function, param) for param in [5, 3]]
    # The order of result values corresponds to the order of futures
    results = loop.run_until_complete(asyncio.gather(*futures, loop=loop))
    loop.close()
    return results
```
