# Quick Example

``` python
import os

def child():
    # actions for child process
    print('\nA new child ',  os.getpid())
    os._exit(0)  

def parent():
    while True:
        newpid = os.fork()
        if newpid == 0:
            # child process will get newpid=0
            child()
        else:
            pids = (os.getpid(), newpid)
            print("parent: %d, child: %d\n" % pids)
        reply = input("q for quit / c for new fork")
        if reply == 'c': 
            continue
        else:
            break
    
parent()
```

> Reference: https://www.python-course.eu/forking.php
