# Deploy Flask in Gunicron

# Build Flask App

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello World"

if __name__ == '__main__':
    app.run(debug=True)
```

This example code can be executed directly.

```shell
python app.py
```

# Setup Gunicorn

## Install Gunicorn

```shell
pip install gunicorn
```

## Start Gunicron with Flask App

```shell
gunicorn app:app -b localhost:8000 &
```


# Reference

- [Deploy flask app with nginx using gunicorn and supervisor](https://medium.com/ymedialabs-innovation/deploy-flask-app-with-nginx-using-gunicorn-and-supervisor-d7a93aa07c18)

