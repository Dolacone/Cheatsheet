import flask
import time


@app.route('/stream_data')
def stream_data():
    def generate():
        # create and return your data in small parts here
        for i in range(100):
            time.sleep(1)
            yield str(i)+"\n"
    return flask.Response(generate(), mimetype="text/plain")


# stream large files
@app.route('/stream_large_files')
def stream_large_files():
    def generate():
        chunk_size=8
        with open('file.txt', 'r') as f:
            for chunk in iter(lambda: f.read(chunk_size), b''):
                yield chunk
    return flask.Response(generate(), mimetype="text/plain")
