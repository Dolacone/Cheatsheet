# BDD

## Introduction

[BDD & CI-CD](https://www.rebaca.com/wp-content/uploads/2017/03/Behavior-Driven-Development-BDDandContinuous-Integration-Delivery-CI-CD.pdf)

- (PO) describe features - 描述產品功能
- (PO/SM) create user stories for features - 描述使用者情境
- (QA) create scenarios for user stories - 轉換為驗收項目
- (QA) describe steps for scenarios - 切分測試步驟
- (Dev) run scenarios with failed result - 執行測試
- (Dev) implement application to fulfill scenario tests - 讓測試通過

### Gherkin

- given: initial state
- when: actions taken
- then: outcome to verify

```gherkin
Given there is one online agent
  And remaining cores is greater than 8
  And no other job is running
When user submit job with 8 cores
  And wait for 10 seconds
Then this job should be in running
```



## Pros

- better CI (real testing)
- bigger scope (simulate user behavior, mostly end-to-end test)
- easily regression test
  - reproduce bugs by test scenario
- early involved for QA & PM in agile process

## Cons

- Gherkin use parsing, hard to remember, too many to implement
  - use YAML parsing might be easier

## Test Coverage

- User Acceptance Testing (UAT)
  - end to end test to see if meets user's expect
- Black Box Testing
  - test with requirements and functionality
- Regression Testing
  - ensure tested items still pass in latest build

## Tools

- [cucumber](https://cucumber.io/)
- [pytest-bdd](https://github.com/pytest-dev/pytest-bdd) - integrated with pytest
- [behave](https://behave.readthedocs.io/en/latest/)

## Implementation (Things to Do)

### BDD framework

- YAML parser (if not using Gherkin)
- steps handler

```gherkin
Given there is one online agent
  And remaining cores is greater than 8
  And no other job is running
When user submit job with 8 cores
  And wait for 10 seconds
Then this job should be in running
```



```yaml
- scenario:
    name: test 01 - job can be running
    given:
      - agents:
          online: 1
      - cores:
          greater_than: 8
      - jobs:
          running: 0
    when:
      - submit_job:
          name: test 01 - job can be running
          command: sleep 30
          cores: 8
          pool: test
      - wait: 10
    then:
      - job:
          name: test 01 - job can be running
          status: running
```



### CI framework

- Jenkins (with PR)

## Reference

- [BDD example for scala](https://www.codacy.com/blog/putting-bdd-in-practice-using-scala/)
