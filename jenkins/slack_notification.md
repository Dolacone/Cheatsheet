# Plugin Setting

## Install Slack Plugin

Go to Jenkins plugins page, find `Slack Notification Plugin` from available plugins and install it.

## Get Slack Token for Jenkins

Go to [Slack Apps Management page](https://www.slack.com/apps/manage), install `Jenkins CI` if first time, then click on `add configuration`.

Remember `Base URL` and `Integration Token` it provided, these will be used in Jenkins configurations.

## Configure Jenkins Global Settings

In Jenkins setting page, update these fields:

- Global Slack Notifier Settings
  - Base URL
  - Integration Token

Leave others as blank, it will use configurations you provided in Jenkins App.

# Trigger Slack Notification

## Function for Pipeline

An additional function is required for pipeline usage, just place it in pipeline script will work.

```groovy
def slack_notify(String buildStatus) {
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESS'

    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} (${env.BUILD_URL})"

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
      color = 'YELLOW'
      colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESS') {
      color = 'GREEN'
      colorCode = '#00FF00'
    } else {
      color = 'RED'
      colorCode = '#FF0000'
    }

    // Send notifications
    slackSend (color: colorCode, message: summary)
}
```

## Jenkinsfile Script (example 1)

Example pipeline script to send Slack notification when completed.

This example is using node with try/catch.

```groovy
node {
    try {
        stage('Prepare code') {
            echo 'do checkout stuff'
        }

        stage('Deploy') {
            echo 'Deploy - Backend'
            echo 'Deploy - Frontend'
        }

    } catch (e) {
        // If there was an exception thrown, the build failed
        currentBuild.currentResult = "FAILED"
        throw e
    } finally {
        // Success or failure, always send notifications
        slack_notify(currentBuild.currentResult)
    }
}
```

## Jenkinsfile Script (example 2)

This example is using pipeline.

```groovy
pipeline {
    agent any
    
    stages{
        stage('Prepare code') {
        	steps{
	            echo 'do checkout stuff'
	        }
        }

        stage('Deploy') {
        	steps{
                echo 'Deploy - Backend'
                echo 'Deploy - Frontend'
	        }
        }
    }

    post{
        always{
            slack_notify(currentBuild.currentResult)
        }
    }
}
```