# Dynamic Parameter Selection

Provide parameter options by restAPI.

# Plugin Installation

Install `Active Choices Plug-in` from Jenkins plugin manager.

# Prepare RestAPI

Provide options by API response.

```python
@get('/api/ami')
def api_ami():
    # ["id A | release date | commit id", "id B | release date | commit id"]
    
    virtualization_type = request.query.get('virtualization_type')
    spark_version = request.query.get('spark_version')
    ami_dict = ec2.get_ami_list(virtualization_type=virtualization_type, spark_version=spark_version)
    amis = ["{} | {} | {}".format(a, ami_dict[a]['release'], ami_dict[a]['commit']) for a in ami_dict]
    amis = sorted(amis, key=lambda a: a.split(' | ')[1], reverse=True)
    return json.dumps(amis)
```

# Jenkins Job Configuration

- add `Active Choices Reactive Parameter`
- select `Groovy Script`
- enter variable to be used in this script in `Referenced parameters`

```groovy
import groovy.json.JsonSlurper

def artifactsUrl = "http://${hostname}/api/ami?virtualization_type=hvm&spark_version=${spark_version}"
def artifactsObjectRaw = ["curl", "-s", "-H", "accept: application/json", "-k", "--url", "${artifactsUrl}"].execute().text
def jsonSlurper = new JsonSlurper()
return jsonSlurper.parseText(artifactsObjectRaw)
```
