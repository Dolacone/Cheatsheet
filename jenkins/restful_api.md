# Credentials

## Add New Credentials

```
curl -u "$USER:$API_TOKEN" -X POST 'http://$HOST:$PORT/credentials/store/system/domain/_/createCredentials' --data-urlencode 'json={
  "": "0",
  "credentials": {
    "scope": "GLOBAL",
    "id": "$CREDENTIAL_ID",
    "username": "$CREDENTIAL_USERNAME",
    "password": "",
    "privateKeySource": {
      "stapler-class": "com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey$DirectEntryPrivateKeySource",
      "privateKey": "ssh-rsa $PRIVATE_KEY",
    },
    "description": "$DESCRIPTION",
    "stapler-class": "com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey"
  }
}'
```


## Update Credentials

config.yml

```
<com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey plugin="ssh-credentials@1.13">
  <scope>GLOBAL</scope>
  <id>CREDENTIALS_ID</id>
  <description>CREDENTIALS_DESCRIPTION</description>
  <username>CREDENTIALS_USERNAME</username>
  <privateKeySource class="com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey$DirectEntryPrivateKeySource">
    <privateKey>
      <secret-redacted/>
    </privateKey>
  </privateKeySource>
</com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey>
```

```
curl -u "$USER:$API_TOKEN" -X POST -H content-type:application/xml -d @config.xml 'http://$HOST:$PORT/credentials/store/system/domain/_/credential/$CREDENTIALS_ID/config.xml'
```


## Delete Credentials


```
curl -u "$USER:$API_TOKEN" -X DELETE 'http://$HOST:$PORT/credentials/store/system/domain/_/credential/$CREDENTIALS_ID/config.xml'
```
