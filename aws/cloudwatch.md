# Amazon CloudWatch

# Feature

## Logs

Collect and store logs.

- AWS services logs
- custom logs generated from own applications

## Metrics

Store filters into metrics, use for dashboards display or alarm rules.

- EC2 instances CPU utilization, data transfer, disk usage
- custom metrics

## Monitors

- dashboards
- alarms

## Actions

Trigger additional actions when metrics reachs criteria.

- auto scaling
- trigger AWS Lamdba

# Pricing

| Type                | Price                  |
| ------------------- | ---------------------- |
| Logs collect        | $0.5 /GB               |
| Logs storage        | $0.03 /GB              |
| Logs insights query | $0.005 /GB scanned     |
| Logs export to S3   | $0.25 /GB              |
| Metrics             | $0.3 / 10,000 metrics  |
| Dashboard           | $3 / dashboard         |
| Alarm               | $0.1 / alarm           |
| API call            | $0.01 / 1,000 requests |

## Sample Pricing

> daily 1G, store for 30 days, query daily data every day

| Type                | Calculate             | Price  |
| ------------------- | --------------------- | ------ |
| Logs collect        | 0.5/G * 1G * 30days   | $15    |
| Logs storage        | 0.03/G * 30G          | $0.9   |
| Logs insight query  | 0.005/G * 1G * 30days | $0.15  |
| Logs per GB (total) |                       | $16.05 |
| Dashboard           |                       | $3     |
| Alert               |                       | $0.1   |

# Custom Logs from Python

## Install CloudWatch Handler

```bash
pip install watchtower
```

## Setup Logging Handler

```yaml
---
version: 1
disable_existing_loggers: False
formatters:
    simple:
        format: "%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s"

handlers:
    console:
        class: logging.StreamHandler
        level: DEBUG
        formatter: simple
        stream: ext://sys.stdout
        
    aws_cloudwatch:
        class: watchtower.CloudWatchLogHandler
        level: DEBUG
        formatter: simple
        log_group: log_group_name
        stream_name: log_stream_name
        send_interval: 1
        create_log_group: True  # True by default
        boto3_profile_name: aws_profile_watchtower  # optional
        
loggers:
    my_logger:
        level: DEBUG
        handlers: [console, aws_cloudwatch]
        propagate: no
```

## Use CloudWatch Handler

```python
import logging.config
import yaml


def get_logger(logger_name='root'):
    log_conf = 'logging.yml'
    with open(log_conf, 'r') as f:
        config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)
    return logging.getLogger(logger_name)

logger = get_logger('my_logger')
logger.debug('debug')
logger.info('info')
logger.warning('warning')
logger.error('error')
```

# Operation with CloudWatch

## Read Logs with awscli

```bash
aws logs get-log-events --log-group-name log_group_name --log-stream-name log_stream_name | jq '.events[].message'
```

## CloudWatch Insights

CloudWatch -> Logs Insights

```
parse @message '* * - * - * - * - *' as date, time, level, code, module, content
| sort @timestamp desc
| filter level == 'ERROR'
| limit 20
```
