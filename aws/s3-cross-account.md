# S3 Cross-Account Access

## Scenario

User in Account_A can use his AWS Credentials to access S3 bucket in Account_B.

- Account_A ID: 111111111111
- bucket name: example-bucket

## Enable Permission on Bucket (Account_B)

Setup bucket policy in S3 bucket permissions tab: (allow Account_A to visit this bucket)

Example for read permission:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "access",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::111111111111:root"
            },
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket"
            ]
        },
        {
            "Sid": "read",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::111111111111:root"
            },
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket/*"
            ]
        }
    ]
}
```

Example for write permission:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "access",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::111111111111:root"
            },
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket"
            ]
        },
        {
            "Sid": "write",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::111111111111:root"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket/*"
            ]
        }
    ]
}
```

Admin in Account_A can now access this bucket.

## Enable Permission on User in Account_A

Assign permission to User_A (in Account_A).

Example for read permission:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "access",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket"
            ]
        },
        {
            "Sid": "write",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket/*"
            ]
        }
    ]
}
```

Example for write permission:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "access",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket"
            ]
        },
        {
            "Sid": "write",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::example-bucket/*"
            ]
        }
    ]
}
```
