# Login Vault with AWS

# Setup AWS Auth

## Create IAM User for Vault

Create IAM user for Vault usage with following permission:

```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstances",
        "iam:GetInstanceProfile",
        "iam:GetUser",
        "iam:GetRole"
      ],
      "Resource": "*"
    }
  ]
}
```

## Enable AWS Auth in Vault

```bash
export VAULT_ADDR=http://{{HOST}}:{{PORT}}
export VAULT_TOKEN={{VAULT_TOKEN}}
./vault auth enable aws
./vault write auth/aws/config/client access_key={{AWS_KEY}} secret_key={{AWS_SECRET}}
```

# Assign Policy for IAM

## Create Policy

Policy file:

```hcl
path "secret/data/example_secret" {
	capabilities = ["read"]
}
```

> Please note that "secret/data/example_secret" stands for "secret/example_secret" during kv usage.

## Import Policy

```bash
./vault policy write example_policy policy_file.hcl
```

## Assign Policy to IAM User

```bash
./vault write auth/aws/role/example_role_iam auth_type=iam bound_iam_principal_arn=arn:aws:iam::{{AWS_ACCOUNT_ID}}:user/{{USER_NAME}} policies=example_policy max_ttl=1m
```

# Get Vault Secrets with AWS IAM

## Write Secret with Root Credential

```bash
./vault kv put secret/example_secret foo=bar

Key              Value
---              -----
created_time     2018-07-12T06:41:09.044578081Z
deletion_time    n/a
destroyed        false
version          1
```

## Login Vault with AWS IAM User

```bash
./vault login -method=aws role=example_role_iam

Key                                Value
---                                -----
token                              a1ce59f1-968e-9f76-1103-ead372927d20
token_accessor                     599132ab-a465-523d-ab6f-0a340ac9b7c1
token_duration                     1m
token_renewable                    true
token_policies                     ["default" "example_policy"]
identity_policies                  []
policies                           ["default" "example_policy"]
token_meta_canonical_arn           arn:aws:iam::{{AWS_ACCOUNT_ID}}:user/example_user
token_meta_client_arn              arn:aws:iam::{{AWS_ACCOUNT_ID}}:user/example_user
token_meta_client_user_id          {{AWS_ACCESS_KEY}}
token_meta_inferred_aws_region     n/a
token_meta_inferred_entity_id      n/a
token_meta_inferred_entity_type    n/a
token_meta_account_id              {{AWS_ACCOUNT_ID}}
token_meta_auth_type               iam
```

## Read Secret

```bash
$ ./vault kv get secret/example_secret

====== Metadata ======
Key              Value
---              -----
created_time     2018-07-12T06:41:09.044578081Z
deletion_time    n/a
destroyed        false
version          1

=== Data ===
Key    Value
---    -----
foo    bar
```

# Assign Policy for EC2

## Create Policy

Policy file:

```hcl
path "secret/data/example_secret" {
	capabilities = ["read"]
}
```

> Please note that "secret/data/example_secret" stands for "secret/example_secret" during kv usage.

## Import Policy

```bash
./vault policy write example_policy policy_file.hcl
```

## Bind AWS IAM Role to Instances

### Create AWS IAM Role

1. Visit [AWS IAM Role Page](https://console.aws.amazon.com/iam/home?#/roles)
2. Create new role with `AWS Service > EC2` as trusted entity (no policy required)

### Assign IAM Role to Instances

1. Visit [AWS EC2 Instances Page](https://console.aws.amazon.com/ec2/v2/home?#Instances)
2. Select instances, and click on `Actions > Instance Settings > Attach/Replace IAM Role`
3. Select the IAM Role created on previous step

## Assign Policy to IAM Role

```bash
./vault write auth/aws/role/example_role_ec2 auth_type=ec2 bound_iam_role_arn=arn:aws:iam::{{AWS_ACCOUNT_ID}}:role/{{AWS_IAM_ROLE}} policies=example_policy max_ttl=1m
```

# Get Vault Secrets with AWS EC2

## Write Secret with Root Credential

```bash
./vault kv put secret/example_secret foo=bar

Key              Value
---              -----
created_time     2018-07-12T06:41:09.044578081Z
deletion_time    n/a
destroyed        false
version          1
```

## Get Vault Token with AWS EC2

Vault is using instance identity (in pkcs7) for authencation.

```bash
# get pkcs7 from AWS dynamic reply server
pkcs7=$( curl -Ss http://169.254.169.254/latest/dynamic/instance-identity/pkcs7 | paste -s -d '' )

# get token from vault server with pkcs7
curl -k -X POST "http://{{VAULT_HOST}}:{{VAULT_PORT}}/v1/auth/aws/login" -d '{"role":"example_role_ec2","pkcs7":"'$pkcs7'", "nonce": "123"}'

{
  "auth":{
    "client_token":"d992bf5c-688d-559d-3619-fbac16ce2cc6",
    "accessor":"02b809c4-f3c8-0691-1fb7-df2f42f2db8b",
    "policies":[
      "default",
      "example_policy_ec2"
    ],
    "metadata":{
      "account_id":"{{AWS_ACCOUNT_ID}}",
      "ami_id":"{{INSTANCE_AMI_ID}}",
      "instance_id":"{{INSTANCE_ID}}",
      "nonce":"123",
      "region":"{{INSTANCE_REGION}}",
      "role":"example_role_ec2",
      "role_tag_max_ttl":"0s"
    },
    "lease_duration":60,
    "renewable":true
  }
}
```

> Nonce is required for reauthentication, set nonce to any string and reuse it when requesting token.  
> Visit [official api document](https://www.vaultproject.io/api/auth/aws/index.html#nonce) for more information.

If nonce is lost, and it returned `{"errors":["reauthentication is disabled"]}` from login API, you will have to manually delete the instance id from Vault.

```bash
./vault delete auth/aws/identity-whitelist/{{INSTANCE_ID}}
```

