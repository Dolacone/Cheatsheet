# Execute Commands in SSH

```bash
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no user@host 'bash' "
    cd /opt
    ls
" 2>&1 | sed "s/^/${host}: /" &
```

# Execute Commands on Multiple Hosts

```bash
hostlist=`cat hosts`
for host in `echo "${hostlist}"|sed "s/#.*$//;/^$/d"`
do
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no user@${host} 'bash' "
        cd /opt
        ls
    " 2>&1 | sed "s/^/${host}: /"
done
```

# Execute Commands on Multiple Hosts Asynchronously

```bash
hostlist=`cat hosts`
for host in `echo "${hostlist}"|sed "s/#.*$//;/^$/d"`
do
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no user@${host} 'bash' "
        cd /opt
        ls
    " 2>&1 | sed "s/^/${host}: /" &
    pids+=($!)
done

echo "waiting for all deployment process to end"
for pid in ${pids[*]}
do
    wait $pid
done
```
