# Check Website Performance

## Output Template

Create a new file `curl_template.txt`, and paste in:

```txt
response: %{http_code}, time: %{time_total}\n
```

Other available variables can be found from [curl man page](https://curl.haxx.se/docs/manpage.html).

## Execute curl

```bash
$ while true; do curl -w "@curl_template.txt" -o /dev/null -s http://localhost:10000/metrics ; done
response: 200, time: 37.393
response: 200, time: 38.122
response: 200, time: 39.384
```

- -w "@curl_template.txt" tells cURL to use our format file
- -o /dev/null redirects the output of the request to /dev/null
- -s tells cURL not to show a progress meter

## Reference

- https://stackoverflow.com/questions/18215389/how-do-i-measure-request-and-response-times-at-once-using-curl
